using Gtk 4.0;
using Adw 1;

menu primary_menu_empty {
  section {
    item {
      /* Translators: Primary menu entry that opens a new window. */
      label: _("_New Window");
      action: "app.new-window";
    }

    item {
      /* Translators: Primary menu entry that opens the media properties window. */
      label: _("_Media Properties");
      action: "win.media-properties";
    }

    item {
      /* Translators: Primary menu entry that opens the keyboard shortcuts window. */
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      /* Translators: Primary menu entry that opens the About dialog. */
      label: _("_About Identity");
      action: "win.about";
    }
  }
}

menu primary_menu_content {
  section {
    item {
      custom: "display-mode-selector";
    }
  }

  section {
    item {
      /* Translators: Primary menu entry that opens a new window. */
      label: _("_New Window");
      action: "app.new-window";
    }

    item {
      /* Translators: Primary menu entry that opens the media properties window. */
      label: _("_Media Properties");
      action: "win.media-properties";
    }

    item {
      /* Translators: Primary menu entry that opens the keyboard shortcuts window. */
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      /* Translators: Primary menu entry that opens the About dialog. */
      label: _("_About Identity");
      action: "win.about";
    }
  }
}

menu tab_menu {
  section {
    item {
      /* Translators: Tab menu entry that copies the file to the clipboard. */
      label: _("Copy File");
      action: "win.copy";
    }

    item {
      /* Translators: Tab menu entry that shows the file in the file manager. */
      label: _("Show in Files");
      action: "win.show-in-files";
    }

    item {
      /* Translators: Tab menu entry that moves it to a new window. */
      label: _("Move to New Window");
      action: "win.move-tab-to-new-window";
    }

    item {
      /* Translators: Tab menu entry that closes it. */
      label: _("Close File");
      action: "win.close-tab";
    }
  }
}

Box display_mode_selector {
  styles ["linked"]
  hexpand: true;
  homogeneous: true;

  ToggleButton tabbed_button {
    icon-name: "tab-symbolic";
    /* Translators: Tooltip for the primary menu button that selects the tabbed display mode. */
    tooltip-text: _("Arrange as Tabs");
    active: true;

    action-target: "\"tabbed\"";
    action-name: "win.set-display-mode";
  }

  ToggleButton row_button {
    icon-name: "horizontal-symbolic";
    /* Translators: Tooltip for the primary menu button that selects the tiled in a row display mode. */
    tooltip-text: _("Arrange in a Row");
    group: tabbed_button;

    action-target: "\"row\"";
    action-name: "win.set-display-mode";
  }

  ToggleButton column_button {
    icon-name: "vertical-symbolic";
    /* Translators: Tooltip for the primary menu button that selects the tiled in a column display mode. */
    tooltip-text: _("Arrange in a Column");
    group: tabbed_button;

    action-target: "\"column\"";
    action-name: "win.set-display-mode";
  }
}

$IdMediaProperties media_properties {
  transient-for: template;
  destroy-with-parent: true;
  hide-on-close: true;
}

template $IdWindow : Adw.ApplicationWindow {
  title: _("Identity");

  /* These parameters are chosen to make the default size of the video 640×360. */
  default-width: 640;
  default-height: 504;

  content: Stack stack {
    transition-type: crossfade;

    notify::visible-child => $on_visible_child_notify() swapped;

    StackPage {
      name: "empty";
      child: Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          title-widget: Adw.WindowTitle {
            title: bind template.title;
          };

          [end]
          MenuButton {
            icon-name: "open-menu-symbolic";
            menu-model: primary_menu_empty;
            primary: true;

            /* Translators: Primary menu tooltip. */
            tooltip-text: _("Main Menu");
          }
        }

        content: Adw.StatusPage {
          vexpand: true;
          icon-name: "identity-symbolic";

          /* Translators: Title text on the window when no files are open. */
          title: _("Compare Media");

          /* Translators: Description text on the window when no files are open. */
          description: _("Drag and drop images or videos here");

          child: Button {
            /* Translators: Open button label when no files are open. */
            label: _("Open Media…");
            /* Translators: Open button tooltip. */
            tooltip-text: _("Select an Image or Video");

            styles ["suggested-action", "pill"]
            halign: center;

            clicked => $on_open_clicked() swapped;
          };
        };
      };
    }

    StackPage {
      name: "content";

      child: Adw.ToolbarView content_toolbar_view {
        top-bar-style: raised_border;
        bottom-bar-style: raised_border;

        [top]
        Adw.HeaderBar {
          [start]
          Button {
            /* Translators: Open button label. */
            label: _("Open");
            /* Translators: Open button tooltip. */
            tooltip-text: _("Select an Image or Video");

            styles ["suggested-action"]

            clicked => $on_open_clicked() swapped;
          }

          title-widget: Adw.WindowTitle {
            title: bind template.title;
          };

          [end]
          MenuButton primary_menu_button_content {
            icon-name: "open-menu-symbolic";
            menu-model: primary_menu_content;
            primary: true;

            /* Translators: Primary menu tooltip. */
            tooltip-text: _("Main Menu");
          }

          [end]
          Box {
            styles ["linked"]

            Entry scale_entry {
              styles ["numeric"]
              max-width-chars: 6;
              width-chars: 6;

              activate => $on_scale_entry_activate() swapped;

              /* Translators: Scale/zoom entry tooltip. */
              tooltip-text: _("Zoom Level");
            }

            MenuButton scale_button {
              /* Translators: Scale/zoom menu button tooltip. */
              tooltip-text: _("Select Zoom Level");
            }
          }
        }

        content: Stack display_mode_stack {
          name: "display-mode-stack";

          // This stack intentionally has no transition. This is because the way
          // the display modes are currently implemented is by removing all
          // pages from the TabView and adding them into the PageGrid. Since
          // Stack keeps drawing the previous visible child during a transition,
          // you see the pages disappear during the crossfade, resulting in a
          // not perfectly smooth animation. Therefore, use no transition until
          // the display mode switching is implemented in a different way.

          StackPage {
            name: "tabbed";

            child: Adw.TabView tab_view {
              vexpand: true;
              menu-model: tab_menu;

              page-attached => $on_tab_page_attached() swapped;
              /* page-detached => $on_tab_page_detached() swapped; */
              create-window => $on_create_window() swapped;
              setup-menu => $on_setup_menu() swapped;
            };
          }

          StackPage {
            name: "grid";

            child: $IdPageGrid page_grid {};
          }
        };

        [bottom]
        Box {
          name: "controls";
          spacing: 6;

          Button play_pause_button {
            icon-name: "media-playback-start-symbolic";
            action-name: "win.play-pause";
          }

          Label time_label {
            styles ["numeric"]
            label: "0:00";

            margin-start: 6; /* Scale and its extra padding... */
          }

          Scale time_scale {
            hexpand: true;

            adjustment: Adjustment time_adjustment {
              upper: 1;
              step-increment: 0.1;
              page-increment: 0.25;
            };
          }
        }
      };
    }
  };
}
