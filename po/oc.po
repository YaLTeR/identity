msgid ""
msgstr ""
"Project-Id-Version: Identity\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/YaLTeR/identity/-/issues\n"
"POT-Creation-Date: 2023-04-12 12:37+0000\n"
"PO-Revision-Date: \n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: \n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.2.2\n"

#: data/org.gnome.gitlab.YaLTeR.Identity.desktop.in.in:3
#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:7
#: data/resources/ui/window.blp:137 src/main.rs:43 src/window.rs:355
msgid "Identity"
msgstr "Identitat"

#: data/org.gnome.gitlab.YaLTeR.Identity.desktop.in.in:4
#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:8
msgid "Compare images and videos"
msgstr "Comparar d’imatges e de vidèos"

#. Translators: search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.YaLTeR.Identity.desktop.in.in:10
msgid "Movie;Film;Clip;Difference;Frame;Quality;"
msgstr "Film;Clip;Diferéncia;Quadre;Frame;Qualitat;"

#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:10
msgid "A program for comparing multiple versions of an image or video."
msgstr "Un programa per comparar mantun version d’un imatge o d’una vidèo."

#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:15
msgid "You can arrange videos in a row"
msgstr ""

#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:19
msgid "You can arrange images in a column"
msgstr ""

#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:23
msgid "You can open files as tabs"
msgstr ""

#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:27
msgid "Inspect a few properties of a video in a dialog"
msgstr ""

#. Translators: name of the developer of the application.
#: data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in:155
#: src/window.rs:360
msgid "Ivan Molodetskikh"
msgstr "Ivan Molodetskikh"

#. Translators: Title of the media properties window.
#: data/resources/ui/media_properties.blp:6
msgid "Properties"
msgstr "Proprietats"

#. Translators: Title text in the media properties window when no files are open.
#: data/resources/ui/media_properties.blp:22
msgid "No Media Open"
msgstr ""

#. Translators: Description on the media properties window when no files are open.
#: data/resources/ui/media_properties.blp:24
msgid "Open a video or image to view its properties."
msgstr ""

#. Translators: Section in the media properties window.
#: data/resources/ui/media_properties.blp:44
msgid "File"
msgstr "Fichièr"

#. Translators: File-name row in the media properties window.
#: data/resources/ui/media_properties.blp:50
msgid "Name"
msgstr "Nom"

#. Translators: File location row in the media properties window.
#: data/resources/ui/media_properties.blp:58
msgid "Location"
msgstr "Emplaçament"

#. Translators: Section in the media properties window.
#: data/resources/ui/media_properties.blp:65
msgid "Media"
msgstr "Mèdia"

#. Translators: Pixel-resolution row in the media properties window.
#: data/resources/ui/media_properties.blp:71
msgid "Resolution"
msgstr "Resolucion"

#. Translators: Frame-rate row in the media properties window.
#: data/resources/ui/media_properties.blp:79
msgid "Frame Rate"
msgstr "Frequéncia d'imatge"

#. Translators: Codec row in the media properties window.
#: data/resources/ui/media_properties.blp:87
msgid "Codec"
msgstr "Codèc"

#. Translators: Container-format row in the media properties window.
#: data/resources/ui/media_properties.blp:95
msgid "Container"
msgstr "Contenedor"

#. Translators: Title that is shown upon video or image loading error.
#: data/resources/ui/page.blp:40
msgid "File Failed to Load"
msgstr "Fracàs de cargament del fichièr"

#. Translators: Label shown when a video or image has failed to load.
#: data/resources/ui/page.blp:43
msgid ""
"Could not display the file.\n"
"\n"
"If you're running Identity under Flatpak, note that opening files by drag-"
"and-drop and by pasting may not work."
msgstr ""
"Afichatge impossible del fichièr.\n"
"\n"
"S'utilizatz Identitat jos Flatpak, notatz que la dubertura en lisar depausar "
"e en pegant poirián foncionar pas."

#: data/resources/ui/shortcuts.blp:6
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/resources/ui/shortcuts.blp:9
msgctxt "shortcut window"
msgid "Switch between files"
msgstr "Bascular entre los fichièrs"

#: data/resources/ui/shortcuts.blp:14
msgctxt "shortcut window"
msgid "Open videos or images"
msgstr "Dobrir las vidèos o los imatges"

#: data/resources/ui/shortcuts.blp:19
msgctxt "shortcut window"
msgid "Open file from clipboard"
msgstr "Dobrir un fichièr a partir del quichapapièr"

#: data/resources/ui/shortcuts.blp:24
msgctxt "shortcut window"
msgid "Copy selected file to clipboard"
msgstr ""

#: data/resources/ui/shortcuts.blp:29
msgctxt "shortcut window"
msgid "Close selected file"
msgstr "Tampar lo fichièr seleccionat"

#: data/resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "New window"
msgstr "Fenèstra novèla"

#: data/resources/ui/shortcuts.blp:39
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr "Afichar los acorchis"

#: data/resources/ui/shortcuts.blp:44
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quitar"

#: data/resources/ui/shortcuts.blp:50
msgctxt "shortcut window"
msgid "Playback"
msgstr "Lectura"

#: data/resources/ui/shortcuts.blp:53
msgctxt "shortcut window"
msgid "Play / Pause"
msgstr "Lectura / pausa"

#: data/resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Step forward one frame"
msgstr "Avançar d’un imatge"

#: data/resources/ui/shortcuts.blp:63
msgctxt "shortcut window"
msgid "Step back one frame"
msgstr "Recular d’un imatge"

#: data/resources/ui/shortcuts.blp:69
msgctxt "shortcut window"
msgid "Zoom"
msgstr "Zoom"

#: data/resources/ui/shortcuts.blp:72 data/resources/ui/shortcuts.blp:82
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Zoom avant"

#: data/resources/ui/shortcuts.blp:77 data/resources/ui/shortcuts.blp:87
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Zoom arrière"

#: data/resources/ui/shortcuts.blp:92
msgctxt "shortcut window"
msgid "Zoom 1:1"
msgstr "Zoom 1:1"

#: data/resources/ui/shortcuts.blp:97
msgctxt "shortcut window"
msgid "Best fit"
msgstr "Talha ideala"

#. Translators: Shortcuts window section title for the display mode (arrange as tabs, in a row, in a column).
#: data/resources/ui/shortcuts.blp:104
msgctxt "shortcut window"
msgid "Display"
msgstr "Afichatge"

#: data/resources/ui/shortcuts.blp:107
msgctxt "shortcut window"
msgid "Arrange as tabs"
msgstr ""

#: data/resources/ui/shortcuts.blp:112
msgctxt "shortcut window"
msgid "Arrange in a row"
msgstr ""

#: data/resources/ui/shortcuts.blp:117
msgctxt "shortcut window"
msgid "Arrange in a column"
msgstr ""

#. Translators: Primary menu entry that opens a new window.
#: data/resources/ui/window.blp:8 data/resources/ui/window.blp:42
msgid "_New Window"
msgstr "Fenèstra _novèla"

#. Translators: Primary menu entry that opens the media properties window.
#: data/resources/ui/window.blp:14 data/resources/ui/window.blp:48
msgid "_Media Properties"
msgstr "_Proprietats del mèdia"

#. Translators: Primary menu entry that opens the keyboard shortcuts window.
#: data/resources/ui/window.blp:20 data/resources/ui/window.blp:54
msgid "_Keyboard Shortcuts"
msgstr "Acorchis de _clavièr"

#. Translators: Primary menu entry that opens the About dialog.
#: data/resources/ui/window.blp:26 data/resources/ui/window.blp:60
msgid "_About Identity"
msgstr "_A prepaus d’Identitat"

#. Translators: Tab menu entry that copies the file to the clipboard.
#: data/resources/ui/window.blp:70
msgid "Copy File"
msgstr "Copiar lo fichièr"

#. Translators: Tab menu entry that shows the file in the file manager.
#: data/resources/ui/window.blp:76
msgid "Show in Files"
msgstr "Mostrar dins los fichièrs"

#. Translators: Tab menu entry that moves it to a new window.
#: data/resources/ui/window.blp:82
msgid "Move to New Window"
msgstr "Desplaçar dins una fenèstra novèla"

#. Translators: Tab menu entry that closes it.
#: data/resources/ui/window.blp:88
msgid "Close File"
msgstr "Tampar lo fichièr"

#. Translators: Tooltip for the primary menu button that selects the tabbed display mode.
#: data/resources/ui/window.blp:102
msgid "Arrange as Tabs"
msgstr ""

#. Translators: Tooltip for the primary menu button that selects the tiled in a row display mode.
#: data/resources/ui/window.blp:112
msgid "Arrange in a Row"
msgstr ""

#. Translators: Tooltip for the primary menu button that selects the tiled in a column display mode.
#: data/resources/ui/window.blp:122
msgid "Arrange in a Column"
msgstr ""

#. Translators: Open button label.
#: data/resources/ui/window.blp:159 data/resources/ui/window.blp:203
msgid "Open"
msgstr "Dobrir"

#. Translators: Open button tooltip.
#: data/resources/ui/window.blp:161 data/resources/ui/window.blp:205
msgid "Select an Image or Video"
msgstr "Seleccionar un imatge o una vidèo"

#. Translators: Primary menu tooltip.
#: data/resources/ui/window.blp:179 data/resources/ui/window.blp:223
msgid "Main Menu"
msgstr "Menú principal"

#. Translators: Text on the window when no files are open.
#: data/resources/ui/window.blp:188
msgid "Open videos or images to compare by pressing the Open button."
msgstr ""
"Dobrissètz las vidèos o los imatges de comparar en quichant lo boton Dobrir."

#. Translators: Scale/zoom entry tooltip.
#: data/resources/ui/window.blp:238
msgid "Zoom Level"
msgstr "Nivèl de zoom"

#. Translators: Scale/zoom menu button tooltip.
#: data/resources/ui/window.blp:243
msgid "Select Zoom Level"
msgstr "Seleccionar lo nivèl d’agrandiment"

#. Translators: "Not applicable" string for the media properties dialog when a
#. given property is unknown or missing (e.g. images don't have frame rate).
#: src/page.rs:274 src/window.rs:475 src/window.rs:483 src/window.rs:491
msgid "N/A"
msgstr ""

#: src/window.rs:335
msgid "Added row and column tiled display modes for side-by-side comparison."
msgstr ""

#: src/window.rs:336
msgid ""
"Changed mouse scroll and hotkey zoom to have consistent speed regardless of "
"the current zoom level."
msgstr ""

#: src/window.rs:337
msgid ""
"Changed mouse scroll to zoom by default instead of panning, and to pan when "
"Ctrl is held."
msgstr ""

#: src/window.rs:338
msgid ""
"Added panning by holding down the left mouse button and dragging when zoomed-"
"in."
msgstr ""

#: src/window.rs:339
msgid "Changed the playback position to update smoothly."
msgstr ""

#: src/window.rs:340
msgid ""
"You can now drag-and-drop a file out of Identity when it is not zoomed-in."
msgstr ""

#: src/window.rs:341
msgid "Added Ctrl+C to copy the current file to clipboard."
msgstr ""

#: src/window.rs:342
msgid "Added a context menu to tabs with a few common actions."
msgstr ""

#: src/window.rs:343
msgid ""
"Optimized video playback performance by enabling OpenGL video processing on "
"compatible setups."
msgstr ""

#: src/window.rs:344
msgid "Updated to the GNOME 44 platform."
msgstr ""

#: src/window.rs:345
msgid "Updated translations."
msgstr "Traduccions actualizadas."

#: src/window.rs:348
msgid ""
"This release adds row and column display modes, reworks mouse gestures and "
"adds drag-and-drop from Identity."
msgstr ""

#. Translators: shown in the About dialog, put your name here.
#: src/window.rs:363
msgid "translator-credits"
msgstr "Quentin PAGÈS"

#. Translators: link title in the About dialog.
#: src/window.rs:369
msgid "Contribute Translations"
msgstr "Participar a las traduccions"

#. Translators: Entry in the scale/zoom menu that indicates that the image or video is
#. always resized to fit the window.
#: src/window.rs:542
msgid "Best Fit"
msgstr "Talha ideala"

#. Translators: file chooser file filter name.
#: src/window.rs:1490
msgid "Videos and images"
msgstr "Vidèos e imatges"

#. Translators: file chooser dialog title.
#: src/window.rs:1501
msgid "Open videos or images to compare"
msgstr "Dobrir las vidèos o los imatges de comparar"

#~ msgid "This release contains translation updates."
#~ msgstr "Aquesta version conten d’actualizacions de traduccion."

#~ msgid "Added Dutch translation (thanks Heimen Stoffels)."
#~ msgstr "Traduccion en neerlandés (mercés Heimen Stoffels)."

#~ msgid "Added French translation (thanks Lapineige)."
#~ msgstr "Traduccion en francés (mercés Lapineige)."

#~ msgid "Added Spanish translation (thanks Óscar Fernández Díaz)."
#~ msgstr "Traduccion en castelhan (mercés Óscar Fernández Díaz)."

#~ msgid "This release fixes minor UI issues."
#~ msgstr "Aquesta version règla de problèmas minors d’interfàcia."

#~ msgid "First release."
#~ msgstr "Primièra version."

#~ msgid "This release contains translation updates and minor UI changes."
#~ msgstr ""
#~ "Aquesta version conten d'actualizacions de traduccions e de cambiaments "
#~ "mendres d'interfàcia."

#~ msgid "Video rotation tag is now respected."
#~ msgstr "L’etiqueta de rotacion de la vidèo es ara observada."

#~ msgid "Added Deutsch translation (thanks Johannes Hayeß)."
#~ msgstr "Traduccion en alemand (mercés Johannes Hayeß)."

#~ msgid "Added Finnish translation (thanks Jiri Grönroos)."
#~ msgstr "Traduccion en finés (mercés Jiri Grönroos)."

#~ msgid "Added Croatian translation (thanks Milo Ivir)."
#~ msgstr "Traduccion en coat (mercés Milo Ivir)."

#~ msgid "Added Italian translation (thanks Blackcat-917)."
#~ msgstr "Traduccion en italian (mercés Blackcat-917)."

#~ msgid "Added Brazilian Portuguese translation (thanks Gustavo Costa)."
#~ msgstr "Traduccion en portugués del Brasil (mercés Gustavo Costa)."

#~ msgid "Added Slovak translation (thanks Jose Riha)."
#~ msgstr "Traduccion en eslovac (mercés Jose Riha)."

#~ msgid "Added Swedish translation (thanks Åke Engelbrektson)."
#~ msgstr "Traduccion en suedés (mercés Åke Engelbrektson)."

#~ msgid "Added translation contribution link."
#~ msgstr "Apondut ligam de contribucion en traduccion."

#~ msgid "Identity is now a GTK 4 application!"
#~ msgstr "Identitat es ara una aplicacion GTK 4 !"

#~ msgid "Rewrote the application with GTK 4 and libadwaita."
#~ msgstr "Reescritura de l’aplicacion amb GTK 4 e libadwaita."

#~ msgid "Changed the default style to dark."
#~ msgstr "Cambiat l'estil per defaut per l'escur."

#~ msgid "Added Basque translation (thanks Sergio Varela)."
#~ msgstr "Traduccion en basc (mercés Sergio Varela)."

#~ msgid "Added Galician translation (thanks Fran Diéguez)."
#~ msgstr "Traduccion en galician (mercés Fran Diéguez)."

#~ msgid "Added Japanese translation (thanks Kaz Sibuya)."
#~ msgstr "Traduccion en japonés (mercés Kaz Sibuya)"

#~ msgid "Added Polish translation (thanks Rafał Baran)."
#~ msgstr "Traduccion en polonés (mercés Rafał Baran)."

#~ msgid "Added Chinese Simplified translation (thanks Garling Liang)."
#~ msgstr "Traduccion en mandarin simplificat (mercés Garling Liang)."

#~ msgid "This release adds zoom support."
#~ msgstr "Aquesta version ajusta l’agrandiment."

#~ msgid "Added Czech translation (thanks Jaroslav Svoboda)."
#~ msgstr "Traduccion en chèc (mercés Jaroslav Svoboda)."
